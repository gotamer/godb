GoDB
====

Written in Go, [GoDB] is a simple, persistent document database.
It stores any Go type as binary values via Gob encoded flatly on the file system.
It has been inspired by the persistent key-value store [Diskv]. 


### GoDB features
 * Dynamic schemas with Go Struct offers simplicity and power.
 * Stores files on the underlying filesystem of any size.
 * In memory cache
 * LLRB key index 


##### Additional thanks to:
 * The [Go] Developers
 * One Peter for [Diskv]
 * Another Peter for [LLRB]
 * [LiteIDE] an open source, cross-platform Go IDE. 

[RoboTamer.com]: http://www.robotamer.com
[RoboTamer]: http://www.robotamer.com
[Go]: http://golang.org
[LiteIDE]: http://code.google.com/p/liteide
[GoDB]: http://bitbucket.org/gotamer/godb
[Diskv]: http://github.com/peterbourgon/diskv
[LLRB]: http://github.com/petar/GoLLRB
