// godb_test.go
package godb

import (
	"bitbucket.org/gotamer/tools"
	"fmt"
	"testing"
)

var key string

const (
	transformBlockSize = 2
)

func BlockTransform(s string) []string {
	sliceSize := len(s) / transformBlockSize
	pathSlice := make([]string, sliceSize)
	for i := 0; i < sliceSize; i++ {
		from, to := i*transformBlockSize, (i*transformBlockSize)+transformBlockSize
		pathSlice[i] = s[from:to]
	}
	return pathSlice
}

type user struct {
	Username string
	Password string
	Email    string
}

func TestPath(t *testing.T) {
	opt := new(Options)
	opt.Bucket = "test"
	opt.BasePath = "/dev/shm" + PS + "godb"
	file := "19982"
	d := New(*opt)
	p := d.pathFor(file)
	f := d.completeFilename(file)
	if p+PS+file != f {
		t.Fatalf("File for Path does not match")
	}
}

func TestPath2(t *testing.T) {
	opt := new(Options)
	opt.BasePath = "/dev/shm" + PS + "godb"
	opt.Bucket = "test2"
	opt.Transform = BlockTransform
	d := New(*opt)
	p := d.pathFor("1998")
	f := d.completeFilename("1998")
	if p != d.BasePath+PS+d.Bucket+PS+"19"+PS+"98" {
		t.Fatalf("File for Path2 p does not match")
	}
	if f != d.BasePath+PS+d.Bucket+PS+"19"+PS+"98"+PS+"1998" {
		t.Fatalf("File for Path2 f does not match")
	}
}

func TestWrite(t *testing.T) {
	usr := &user{"Username", "Secret", "user@example.us"}
	opt := new(Options)
	opt.BasePath = "/dev/shm" + PS + "godb"
	opt.Bucket = "test"
	DB := New(*opt)
	if err := DB.PurgeBucket(); err != nil {
		t.Fatalf("Purge bucket failed: %s\n", err.Error())
	}
	key = "testkey"
	key, err := DB.Write(key, usr)
	if err != nil {
		t.Fatalf("Write Error: %s", err)
	}
	if key != "testkey" {
		t.Fatalf("Key Check: %s", key)
	}
}

func TestRead(t *testing.T) {
	key = "testkey"
	usr := new(user)
	opt := new(Options)
	opt.BasePath = "/dev/shm" + PS + "godb"
	opt.Bucket = "test"
	DB := New(*opt)
	_, err := DB.Read(key, usr)
	if err != nil {
		t.Fatalf("Read Error: %s", err)
	}
}

func TestWriteFast(t *testing.T) {
	opt := new(Options)
	opt.BasePath = "/dev/shm" + PS + "godb"
	opt.Bucket = "test"
	DB := New(*opt)
	for i := 1000; i < 2000; i++ {
		k := fmt.Sprintf("%d", i)
		_, err := DB.Write(k, tools.SRand(10, 10, false))
		if err != nil {
			t.Fatalf("WriteFast Error at key: %s -> %s", k, err)
		}
	}
}

func TestErase(t *testing.T) {
	key := "1999"
	opt := new(Options)
	opt.BasePath = "/dev/shm" + PS + "godb"
	opt.Bucket = "test"
	DB := New(*opt)
	err := DB.Erase(key)
	if err != nil {
		t.Fatalf("Erase Error: %s", err)
	}
}

func TestPrugeBucket(t *testing.T) {
	opt := new(Options)
	opt.BasePath = "/dev/shm" + PS + "godb"
	opt.Bucket = "test"
	DB := New(*opt)
	err := DB.PurgeBucket()
	if err != nil {
		t.Fatalf("Purge Bucket Error: %s", err)
	}

}
