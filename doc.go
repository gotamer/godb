package godb

/*
GoDB
====

Written in Go, [GoDB] is a simple, persistent document database.
It stores any Go type as binary values via Gob encoded flatly on the file system.

### GoDB features
 * Dynamic schemas with Go Struct offers simplicity and power.
 * In memory cache


[RoboTamer.com]: http://www.robotamer.com
[RoboTamer]: http://www.robotamer.com
[Go]: http://golang.org
[LiteIDE]: http://code.google.com/p/liteide
[GoDB]: http://bitbucket.org/gotamer/godb
[Diskv]: http://github.com/peterbourgon/diskv
[LLRB]: http://github.com/petar/GoLLRB

*/
