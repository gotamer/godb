// Written in Go, GoDB is a simple, persistent document database with mem cache.
// It stores any Go type as binary values Gob encoded flatly on the file system.
// All keys and some extra info as well as smaller values are chached for fast access.
// There are settings for max cache size.
// The values of the older items get purched from the cache when max cache is reached, but not the keys.
// This is a BETA version, any help is welcome
package godb

import (
	"encoding/gob"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const (
	PS                          = string(os.PathSeparator)
	defaultFilePerm os.FileMode = 0660
	defaultPathPerm os.FileMode = 0770
	defaultBucket               = "default"
)

var (
	defaultBasePath  = os.TempDir() + PS + "godb"
	defaultTransform = func(s string) []string { return []string{} }
)

// Options define a set of properties that dictate GoDB behavior.
// All values have dafaults, and are optional.
type Options struct {
	BasePath         string
	PathPerm         os.FileMode
	FilePerm         os.FileMode
	Transform        TransformFunction
	Bucket           string
	CacheSizeMax     int64 // bytes
	CacheSizeMaxItem int64 // bytes
	CacheLoopSleep   time.Duration
}

// GoDB implements the GoDB interface. You shouldn't construct GoDB
// structures directly; instead, use the New constructor.
type GoDB struct {
	sync.RWMutex
	Options
	cache cache
}

// A TransformFunc transforms a key into a slice of strings, with each
// element in the slice representing a directory in the file path
// where the key's entry will eventually be stored.
//
// For example, if TransformFunc transforms "abcdef" to ["ab", "cde", "f"],
// the final location of the data file will be <basedir>/ab/cde/f/abcdef
type TransformFunction func(s string) []string

// New returns an initialized GoDB structure, ready to use.
// If the path identified by BaseDir already contains GoDB data, it will be accessible.
func New(options Options) *GoDB {
	if options.BasePath == "" {
		options.BasePath = defaultBasePath
	}
	if options.PathPerm == 0 {
		options.PathPerm = defaultPathPerm
	}
	if options.FilePerm == 0 {
		options.FilePerm = defaultFilePerm
	}
	if options.Transform == nil {
		options.Transform = defaultTransform
	}
	if options.Bucket == "" {
		options.Bucket = defaultBucket
	}
	if options.CacheSizeMax == 0 {
		options.CacheSizeMax = defaultCacheSizeMax
	}
	if options.CacheSizeMaxItem == 0 {
		options.CacheSizeMaxItem = defaultCacheSizeMax / 10
	}
	if options.CacheLoopSleep == 0 {
		options.CacheLoopSleep = defaultCacheLoopSleep
	}
	d := &GoDB{
		Options: options,
	}
	d.cache = buildIndex(d.BasePath)
	d.cLoop()
	return d
}

// write synchronously writes the key-object pair to disk,
// making it immediately available for reads.
func (d *GoDB) Write(key string, object interface{}) (string, error) {
	if len(key) <= 0 {
		return key, fmt.Errorf("Empty key")
	}

	d.Lock()
	defer d.Unlock()

	if err := d.ensurePath(key); err != nil {
		return key, fmt.Errorf("ensure path: %s", err)
	}

	f, err := os.OpenFile(d.completeFilename(key), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, d.FilePerm)
	if err != nil {
		return key, fmt.Errorf("Open file: %s", err)
	}
	defer f.Close()

	if err := gob.NewEncoder(f).Encode(object); err != nil {
		return key, fmt.Errorf("Gob encode: %s", err)
	}

	if err := f.Close(); err != nil {
		return key, fmt.Errorf("File close: %s", err)
	}
	//	fmt.Println(object)
	return key, nil
}

func (d *GoDB) Read(key string, o interface{}) (interface{}, error) {
	var err error
	d.RLock()
	defer d.RUnlock()

	// Get from cache
	if o, ok := d.cache.Get(key); ok {
		return o, err
	}

	filename := d.completeFilename(key)

	fileinfo, err := os.Stat(filename)
	if err != nil {
		return o, err
	}

	if fileinfo.IsDir() {
		err = os.ErrNotExist
		return o, err
	}

	f, err := os.Open(filename)
	if err != nil {
		return o, err
	}
	defer f.Close()

	decoder := gob.NewDecoder(f)
	err = decoder.Decode(o)
	if err != nil {
		return o, err

	}
	d.cache.Add(fileinfo, o)

	// TODO start cache loop
	return o, err
}

// Has returns true if the given key exists.
func (d *GoDB) Has(key string) bool {
	return d.cache.Has(key)
}

// Returns FileInfo, which includes Stats like ModTime
func (d *GoDB) Stat(key string) (os.FileInfo, error) {
	filename := d.completeFilename(key)
	return os.Stat(filename)
}

// Erase erases the given key from the disk.
func (d *GoDB) Erase(key string) (err error) {
	var has bool
	d.Lock()
	defer d.Unlock()

	if ok := d.cache.Del(key); ok {
		has = true // Key has been deleted
	}

	filename := d.completeFilename(key)
	var stat os.FileInfo

	if has == false {
		stat, err = os.Stat(filename)
		if err != nil {
			return err
		} else if d := stat.IsDir(); d {
			return fmt.Errorf("Bad key, points to dir not file")
		} else {
			has = true
		}
	}

	if has == true {
		if err = os.Remove(filename); err != nil {
			return err
		}
		// clean up
		d.pruneDirs(key)
	}
	return nil
}

// pruneDirs deletes empty directories in the path walk leading to the key.
// Typically this function is called after an Erase is made.
func (d *GoDB) pruneDirs(key string) error {
	pathlist := d.Transform(key)
	for i := range pathlist {
		pslice := pathlist[:len(pathlist)-i]
		dir := fmt.Sprintf(
			"%s%s%s%s%s",
			d.BasePath,
			PS,
			d.Bucket,
			PS,
			strings.Join(pslice, PS),
		)

		// thanks to Steven Blenkinsop for this snippet
		switch fi, err := os.Stat(dir); true {
		case err != nil:
			return err
		case !fi.IsDir():
			panic(fmt.Sprintf("corrupt dirstate at %s", dir))
		}

		nlinks, err := filepath.Glob(fmt.Sprintf("%s%s*", dir, PS))
		if err != nil {
			return err
		} else if len(nlinks) > 0 {
			return nil // has subdirs -- do not prune
		}
		if err = os.Remove(dir); err != nil {
			return err
		}
	}
	return nil
}

// PurgeBucket will delete a complete collection from the store,
// both in the cache and on the disk.
func (d *GoDB) PurgeBucket() error {
	d.Lock()
	defer d.Unlock()
	d.cache = cache{}
	os.RemoveAll(d.BasePath + PS + d.Bucket)
	fmt.Println("RemoveAll: ", d.BasePath+PS+d.Bucket)
	return nil
}

// pathFor returns the absolute path for location on the filesystem where the
// data for the given key will be stored.
func (d *GoDB) pathFor(key string) string {
	p := fmt.Sprintf(
		"%s%c%s%c%s",
		d.BasePath,
		os.PathSeparator,
		d.Bucket,
		os.PathSeparator,
		strings.Join(d.Transform(key), PS),
	)
	if p[len(p)-1] == os.PathSeparator {
		p = p[0 : len(p)-1]
	}
	return p
}

// ensureDir is a helper function that generates all necessary directories on
// the filesystem for the given key.
func (d *GoDB) ensurePath(key string) error {
	return os.MkdirAll(d.pathFor(key), d.PathPerm)
}

// completeFilename returns the absolute path to the file for the given key.
func (d *GoDB) completeFilename(key string) string {
	return fmt.Sprintf("%s%s%s", d.pathFor(key), PS, key)
}

// Cache maintnance loop
func (d *GoDB) cLoop() {
	go func() {
		for {
			time.Sleep(d.Options.CacheLoopSleep * time.Second)
		}
	}()
}

// Returns a channel that will yield every FileInfo accessible by the store in
// undefined order from the file system.
func Info(path string) <-chan os.FileInfo {
	c := make(chan os.FileInfo)
	go func() {
		filepath.Walk(path, walker(c))
		close(c)
	}()
	return c
}

// walker returns a function which satisfies the filepath.WalkFunc interface.
// It sends every non-directory file entry down the channel c.
func walker(c chan os.FileInfo) func(path string, info os.FileInfo, err error) error {
	return func(path string, info os.FileInfo, err error) error {
		if err == nil && !info.IsDir() {
			c <- info
		}
		return nil // "pass"
	}
}
