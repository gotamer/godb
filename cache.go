// iter
//package main

package godb

import (
	"errors"
	"fmt"
	"os"
	"sync"
	"time"
)

const (
	defaultCacheSizeMax   = 1024 * 1024 * 10 // 10 MB
	defaultCacheLoopSleep = 1                // seconds
)

/*
func main() {
	path := "/home/tamer/code/go/src/test"
	idx := buildIndex(path)
	fmt.Println(len(idx.Name))
	fmt.Println(idx.GetOldest())
}
*/
type cache struct {
	sync.RWMutex
	Name        []string
	Value       []interface{}
	Accessed    []time.Time
	Modified    []time.Time
	Size        []int64
	BasePath    string // Root folder location
	SizeMax     int64
	SizeMaxItem int64
}

// Get returns an object by it's file name
func (c *cache) Get(s string) (o interface{}, ok bool) {
	if i := c.GetKey(s); i >= 0 {
		c.SetAccessed(i)
		c.Lock()
		defer c.Unlock()
		if o = c.Value[i]; o != nil {
			ok = true
		}
	}
	return
}

func (c *cache) Add(info os.FileInfo, o interface{}) {
	if c.SizeMaxItem > info.Size() {
		o = nil
	}
	k := c.GetKey(info.Name())
	c.Lock()
	defer c.Unlock()
	if k < 0 {
		c.Name = append(c.Name, info.Name())
		c.Value = append(c.Value, o)
		c.Modified = append(c.Modified, info.ModTime())
		c.Accessed = append(c.Accessed, time.Now().UTC())
		c.Size = append(c.Size, info.Size())
	} else {
		c.Value[k] = o
		c.Modified[k] = info.ModTime()
		c.Accessed[k] = time.Now().UTC()
		c.Size[k] = info.Size()
	}
}

func (c *cache) Has(s string) (ok bool) {
	if k := c.GetKey(s); k >= 0 {
		ok = true
	}
	return
}

func (c *cache) GetKey(s string) (k int) {
	c.RLock()
	defer c.RUnlock()
	for k, v := range c.Name {
		if v == s {
			return k
		}
	}
	return -1
}

func (c *cache) Del(s string) (ok bool) {
	if i := c.GetKey(s); i >= 0 {

		c.Lock()
		defer c.Unlock()

		cName := c.Name
		cValue := c.Value
		cSize := c.Size
		cAccessed := c.Accessed
		cModified := c.Modified

		n := len(cName)
		if i > -1 && i < n {
			copy(cName[i:n-1], cName[i+1:n])
			c.Name = cName[:n-1]
			copy(cValue[i:n-1], cValue[i+1:n])
			c.Value = cValue[:n-1]
			copy(cSize[i:n-1], cSize[i+1:n])
			c.Size = cSize[:n-1]
			copy(cAccessed[i:n-1], cAccessed[i+1:n])
			c.Accessed = cAccessed[:n-1]
			copy(cModified[i:n-1], cModified[i+1:n])
			c.Modified = cModified[:n-1]
			ok = true
		}
	}
	return
}

func (c *cache) TotalSize() (ts int64) {
	c.RLock()
	defer c.RUnlock()
	for k, v := range c.Size {
		if c.Value[k-1] != nil {
			ts = ts + v
		}
	}
	return
}

func (c *cache) GetName(k int) (string, error) {
	c.RLock()
	defer c.RUnlock()
	if len(c.Name)-1 < k {
		return "", errors.New("Key out of range")
	}

	return c.Name[k], nil
}

func (c *cache) GetValue(k int) (interface{}, error) {
	c.RLock()
	defer c.RUnlock()
	if len(c.Name)-1 < k {
		return "", errors.New("Key out of range")
	}

	return c.Value[k], nil
}

func (c *cache) GetSize(k int) (int64, error) {
	c.RLock()
	defer c.RUnlock()
	if len(c.Name)-1 < k {
		return -1, errors.New("Key out of range")
	}
	return c.Size[k], nil
}

func (c *cache) GetModified(k int) (time.Time, error) {
	c.RLock()
	defer c.RUnlock()
	if len(c.Name)-1 < k {
		return time.Time{}, errors.New("Key out of range")
	}
	return c.Modified[k], nil
}

func (c *cache) SetAccessed(ik int) {
	c.Lock()
	defer c.Unlock()
	c.Accessed[ik] = time.Now().UTC()
}

func (c *cache) GetAccessed(k int) (time.Time, error) {
	c.RLock()
	defer c.RUnlock()
	if len(c.Name)-1 < k {
		return time.Time{}, errors.New("Key out of range")
	}
	return c.Accessed[k], nil
}

// Get the one that has been accessed last not latest
func (c *cache) GetOldest() int {
	var key int
	var oldest time.Duration
	c.RLock()
	defer c.RUnlock()
	for k, v := range c.Accessed {
		if c.Value[k] == nil {
			continue
		}
		since := time.Now().Sub(v)
		if since > oldest {
			key = k
			oldest = since
		}
	}
	return key
}

// rebuildIndex does the work of regenerating the index
// with the given keys.
func buildIndex(path string) cache {
	idx := cache{}
	idx.BasePath = path
	for file := range Info(path) {
		idx.Name = append(idx.Name, file.Name())
		idx.Value = append(idx.Value, nil)
		idx.Modified = append(idx.Modified, file.ModTime())
		idx.Accessed = append(idx.Accessed, file.ModTime())
		idx.Size = append(idx.Size, file.Size())
	}
	return idx
}

// Cache maintnance loop
/*
TODO
1 Sort items by Accessed dec
2 Check from oldest down and delete the first that is much larger then
  average size.

TODO Don't run if read() hasn't been called
*/
func (c *cache) loop() {
	go func() {
		for {
			time.Sleep(1 * time.Second)

			if c.TotalSize() > (c.SizeMax / 100 * 95) {
				var key int
				var oldest time.Duration
				for k, v := range c.Accessed {
					since := time.Now().Sub(v)
					if since > oldest {
						key = k
						oldest = since
					}
				}
				c.Value[key] = nil

				// Optimize SizeMaxItem
				c.SizeMaxItem = c.SizeMax / 100 * 80 / int64(len(c.Name))
				if c.SizeMaxItem > c.SizeMax/100*10 || c.SizeMaxItem < 1 {
					c.SizeMaxItem = c.SizeMax / 100 * 10
				}
				fmt.Println("SizeMaxItem: ", c.SizeMaxItem)
			}
		}
	}()
}
